# docker-tomcat-tutorial
A basic tutorial about running an example web app on Tomcat in a docker container


# Steps
* Install [Docker](https://docs.docker.com/install/).
* Clone this repository - $git clone https://gitlab.com/ozkan32/docker-tomcat-sample.git
* cd 'docker-tomcat-example'
* $docker build -t tomcatwebapp .
* $docker run -p 80:8080 tomcatwebapp
* http://localhost:80

# Links
[Sample Tomcat web app](https://tomcat.apache.org/tomcat-8.0-doc/appdev/sample/)
