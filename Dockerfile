FROM tomcat:8.0-alpine
LABEL maintainer="ozkanselvan@gmail.com"


COPY tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml
COPY Context.xml /usr/local/tomcat/webapps/manager/META-INF/context.xml
ADD sample.war /usr/local/tomcat/webapps/

EXPOSE 8080
CMD ["catalina.sh", "run"]
